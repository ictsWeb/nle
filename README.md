
## notes, links and other stuff
 
 Demo project to *experiment* with frontend and backend technology.
 
 | sub      | feature    |
 | -------- |:----------:|
 | backend  | web / rest |
 | frontend | (web) ui   |
 | admin    | -=-        |
 