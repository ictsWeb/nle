import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { NotesComponent } from './notes/notes.component';
import { AddNoteComponent } from './notes/add-note/add-note.component';
import { ListComponent } from './notes/list/list.component';
import { ApiService } from "./services/api.service";
import { HttpClient, HttpClientModule } from "@angular/common/http";


@NgModule({
  declarations: [
    AppComponent,
    NotesComponent,
    AddNoteComponent,
    ListComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [
    HttpClient,
    ApiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
