import { Note } from "../../shared/note";

class NoteClass implements Note {

    constructor (public id: number,
                 public title: string,
                 public content: string) {

    }

}
