import { Component, OnInit } from '@angular/core';
import { ApiService } from "../../services/api.service";
import { Note } from "../../../shared/note";


@Component({
    selector: 'notes-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

    notes: Note[];

    constructor (private apiService: ApiService) {
    }

    ngOnInit () {
        // const r = this.apiService.httpGetNotes();
        this.apiService.httpGetNotes().subscribe(
            notesResponse => {
                this.notes = notesResponse as any;
            }, error => {
                console.warn(`apiService: get notes fails; ${error}`);

            }, () => {
                console.info('apiService: get notes completed.')
            } );
    }

}
