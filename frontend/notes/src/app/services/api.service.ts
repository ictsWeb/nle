import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Note } from '../../shared/note';
import { Observable } from "rxjs/Observable";

@Injectable()
export class ApiService {

    //private _baseUrl: string = "http://localhost:1111/api";
    private _baseUrl: string = "/api";


    constructor (private httpClient: HttpClient) {
    }

    httpGetNotes () {

        return this.httpClient.get(`${this._baseUrl}/note`);
    }

}
