package net.tbhes.web.service.access.service;

import lombok.extern.slf4j.Slf4j;
import net.tbhes.web.service.access.entity.Account;
import net.tbhes.web.service.access.repo.AccountRepository;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
public class AccountDetailsService implements UserDetailsService {

    private AccountRepository accountRepository;

    public AccountDetailsService (AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.debug("load userName: " + username);
        UserDetails userDetails;
//        Optional<Account> optionalAccount = accountRepository.findByUsername("user");
//        if ( optionalAccount.isPresent() ) {
//            userDetails = new User("user", "password", true, true, true, true, AuthorityUtils.createAuthorityList("ROLE_USER", "ROLE_ADMIN"));
//        } else {
//            throw new UsernameNotFoundException("username NOT found");
//        }
        userDetails = new User("user", "password", true, true, true, true, AuthorityUtils.createAuthorityList("ROLE_USER", "ROLE_ADMIN"));
        return userDetails;
    }

}
