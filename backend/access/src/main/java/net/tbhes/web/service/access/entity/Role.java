package net.tbhes.web.service.access.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.Set;


@NoArgsConstructor
@Data
@Entity
public class Role {

    @Id
    @GeneratedValue
    private long id;
    @Min(3) @Max(256)
    private String role;

    //Set<Account> accounts;

}
