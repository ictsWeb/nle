package net.tbhes.web.service.access.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RequestMapping("/api")
@RestController
public class ApiController {

    @GetMapping("/hello/{name}")
    public String hello (@PathVariable String name) {
        return "\nHello " + name + "!\n\n";
    }

    @GetMapping("/stamp")
    public String stamp () {
        return Long.toString(new Date().getTime()) + "\n";
    }

}
