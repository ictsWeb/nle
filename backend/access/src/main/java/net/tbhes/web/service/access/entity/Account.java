package net.tbhes.web.service.access.entity;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.Set;

@Data
@NoArgsConstructor
@Entity
public class Account {

    @Id @GeneratedValue
    private long id;
    @Min(3) @Max(256)
    private String username;
    @Min(3) @Max(256)
    private String password;
    private String email;
    private boolean active;

    //Set<Role> roles;

    @Builder
    public Account (String username, String password, String email) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.active = true;
    }

}
