package net.tbhes.web.service.access.config;

import net.tbhes.web.service.access.service.AccountDetailsService;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;

@Configuration
@EnableAuthorizationServer
public class AuthServerConfiguration extends AuthorizationServerConfigurerAdapter {

    private TokenStore tokenStore = new InMemoryTokenStore();
    private AuthenticationManager authenticationManager;
    private AccountDetailsService accountDetailsService;

    public AuthServerConfiguration(AuthenticationManager authenticationManager, AccountDetailsService accountDetailsService) {
        this.authenticationManager = authenticationManager;
        this.accountDetailsService = accountDetailsService;
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints.authenticationManager(authenticationManager).tokenStore(tokenStore).userDetailsService(accountDetailsService);
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory()
                .withClient("nle_apps")
                .secret("abcdefgHIJKLMNOPqrstUVWxyz")
                .scopes("nle_ids", "read_only", "read_write")
                .authorizedGrantTypes("password", "refresh_token", "client_credentials")
                .resourceIds("nle_initial", "nle_note", "nle_link");
    }

}
