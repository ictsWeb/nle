package org.example.web.playground.notes.repository;

import org.example.web.playground.notes.models.Link;
import org.springframework.data.repository.CrudRepository;

public interface LinkRepository extends CrudRepository<Link, Long>{

}
