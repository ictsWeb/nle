package org.example.web.playground.notes;

import lombok.extern.java.Log;
import org.example.web.playground.notes.models.Link;
import org.example.web.playground.notes.models.Note;
import org.example.web.playground.notes.service.LinkService;
import org.example.web.playground.notes.service.NoteService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@Log
@SpringBootApplication
public class NotesApplication {

    public static void main (String[] args) {
        ApplicationContext appContext = SpringApplication.run(NotesApplication.class, args);

        log.info("beans count: " + appContext.getBeanDefinitionCount());
    }

    @Bean
    CommandLineRunner commandLineRunner (NoteService noteService, LinkService linkService) {
        return args -> {
            //
            noteService.save(new Note(1L, "myNote", "bla bla"));
            noteService.save(new Note(2L, "no rules", "We used to be gorillas. All we had is what we could take and defend.."));
            noteService.save(new Note(3L, "roads", "Maps used to say, 'There be dragons here.' Now they don’t."));
            //
            linkService.save(new Link(1L, "howdy", "http://www.example.org/aap/noot/mies", "_blank"));
            linkService.save(new Link(2L, "hi", "http://www.example.net/wim/zus/jet", "_self"));
            linkService.save(new Link(3L, "hes", "http://hes.xs4all.nl/tbh", "_blank"));
        };
    }

}
