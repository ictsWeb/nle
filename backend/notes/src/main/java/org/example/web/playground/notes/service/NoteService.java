package org.example.web.playground.notes.service;

import org.example.web.playground.notes.models.Note;

public interface NoteService {

    Iterable<Note> list();

    Note save(Note note);

}
