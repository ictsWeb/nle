package org.example.web.playground.notes.repository;

import org.example.web.playground.notes.models.Note;
import org.springframework.data.repository.CrudRepository;

public interface NoteRepository extends CrudRepository<Note, Long>{

}
