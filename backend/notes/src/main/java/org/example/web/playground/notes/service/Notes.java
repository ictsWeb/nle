package org.example.web.playground.notes.service;

import org.example.web.playground.notes.models.Note;
import org.example.web.playground.notes.repository.NoteRepository;
import org.springframework.stereotype.Service;

@Service
public class Notes implements NoteService {

    private NoteRepository noteRepository;

    public Notes(NoteRepository noteRepository) {
        this.noteRepository = noteRepository;
    }

    @Override
    public Iterable<Note> list () {
        return this.noteRepository.findAll();
    }

    @Override
    public Note save (Note link) {
        return this.noteRepository.save(link);
    }

}
