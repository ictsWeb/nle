package org.example.web.playground.notes.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class Hello {

	@RequestMapping(value = "/home", method = RequestMethod.GET )
	String getHome() {
		return "home";
	}

}
