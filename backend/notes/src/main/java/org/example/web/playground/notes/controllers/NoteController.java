package org.example.web.playground.notes.controllers;

import org.example.web.playground.notes.models.Note;
import org.example.web.playground.notes.service.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/note")
public class NoteController {

    @Autowired
    private NoteService noteService;

    public NoteController(NoteService noteService) {
        this.noteService = noteService;
    }

    @GetMapping(value = {"", "/"})
    public Iterable<Note> list() {
        return this.noteService.list();
    }

    @PostMapping("/save")
    public Note save (@RequestBody Note note) {
        return this.noteService.save(note);
    }

}
