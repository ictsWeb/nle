package org.example.web.playground.notes.service;

import org.example.web.playground.notes.models.Link;
import org.example.web.playground.notes.repository.LinkRepository;
import org.springframework.stereotype.Service;

@Service
public class Links implements LinkService {

    private LinkRepository linkRepository;

    public Links (LinkRepository linkRepository) {
        this.linkRepository = linkRepository;
    }

    @Override
    public Iterable<Link> list () {
        return this.linkRepository.findAll();
    }

    @Override
    public Link save (Link link) {
        return this.linkRepository.save(link);
    }

}
