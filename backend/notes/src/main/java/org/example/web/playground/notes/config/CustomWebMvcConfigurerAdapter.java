package org.example.web.playground.notes.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * see also: https://stackoverflow.com/questions/28437314/spring-boot-doesnt-map-folder-requests-to-index-html-files/29969014
 */


@Configuration
public class CustomWebMvcConfigurerAdapter extends WebMvcConfigurerAdapter {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/app").setViewName("redirect:/app/");
        registry.addViewController("/app/").setViewName("forward:/app/index.html");
        super.addViewControllers(registry);
    }

}
