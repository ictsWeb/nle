package org.example.web.playground.notes.controllers;

import org.example.web.playground.notes.models.Link;
import org.example.web.playground.notes.service.LinkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/link")
public class LinkController {

    @Autowired
    private LinkService linkService;

    public LinkController (LinkService linkService) {
        this.linkService = linkService;
    }

    @GetMapping(value = {"", "/"})
    public Iterable<Link> listNotes() {
        return this.linkService.list();
    }

    @PostMapping("/save")
    public Link save (@RequestBody Link link) {
        return this.linkService.save(link);
    }

}
