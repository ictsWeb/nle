package org.example.web.playground.notes.models;

import lombok.*;

import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomLink {

    public enum Protocol {
        http,
        secure,
        same,
        mail,
        file,
        data;


        @Override
        public String toString () {
            switch (this) {
                case http:
                    return "http:/";
                case secure:
                    return "https:/";
                case same:
                    return "/";
                case mail:
                    return "mailTo:/";
                case file:
                    return "file:/";
                case data:
                    return "data:/";
            }
            return "/";
        }
    }

    boolean relativeLink;

    Protocol proto;

    @Getter
    @Setter
    String hostname;

    Integer port;

    String path;

    Map<String, String> parameters;

    String fragment;

    @Override
    public String toString () {
        String hostPart;
        String pathPart;

        if ( parameters == null) {
            pathPart = path;
        } else {
            String params = "";
//            parameters.forEach() {
//
//            }
            pathPart = String.join("?", path, params);
            if (fragment != null) {
                pathPart += '#' + fragment;
            }
        }

        if ( relativeLink ) {
            return pathPart;
        } else {
            if (port == null) {
                hostPart = proto.toString() + '/' + hostname;
            } else {
                hostPart = proto.toString() + '/' + hostname + ':' + port.toString();
            }
        }

        return hostPart + '/' + pathPart;
    }

}
