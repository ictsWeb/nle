package org.example.web.playground.notes.service;

import org.example.web.playground.notes.models.Link;

public interface LinkService {

    Iterable<Link> list();

    Link save(Link link);

}
