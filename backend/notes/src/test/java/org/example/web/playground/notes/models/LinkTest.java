package org.example.web.playground.notes.models;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class LinkTest {

    public Link l1 = null;
    public Link l2 = null;
    public static String name1 = "l1";
    public static String url1 = "https://www.example.org/some/path";
    public static String target1 = "_blank";
    public static String extraInfo_key1 = "aap";
    public static String extraInfo_value1 = "aap";
    public static String extraInfo_key2 = "noot";
    public static String extraInfo_value2 = "aapNootMies";
    public static String extraInfo_key3 = "mies";
    public static String extraInfo_value3 = "abcDEFghi";
    public static String name2 = "link2";
    public static String url2 = "http://www.info.net/some/other/path";
    public static String target2 = "_self";


    @Before
    public void setUp() throws Exception {
        l1 = new Link();
        l1.setDisplayName(name1);
        l1.setUrl(url1);
        l1.setTarget(target1);
        Map<String, String> moreInfo = new HashMap<>();
        moreInfo.put(extraInfo_key1, extraInfo_value1);
        moreInfo.put(extraInfo_key2, extraInfo_value2);
        moreInfo.put(extraInfo_key3, extraInfo_value3);
        //l1.setExtraInfo(moreInfo);
        //
        l2 = new Link();
        l2.setDisplayName(name2);
        l2.setUrl(url2);
        l2.setTarget(target2);
    }

    @Test
    public void getDisplayName() {
        String expectedDisplayName = name1;
        //expectedDisplayName = "link1";
        //assertEquals(l1.getDisplayName(), expectedDisplayName);
        Assert.assertEquals(l1.getDisplayName(), expectedDisplayName);
        //
        assertEquals(l2.getDisplayName(), name2);
    }

    @Test
    public void getUrl() throws Exception {
        String expectedUrl = url1;
        //expectedUrl = url2;
        assertEquals(l1.getUrl(), expectedUrl);
        assertEquals(l2.getUrl(), url2);
    }

//    @Test
//    public void getExtraInfo1() {
//        String expectedInfo = extraInfo_value1;
//        //expectedInfo = extraInfo_value2;
//        assertEquals(l1.getExtraInfo().get(extraInfo_key1), expectedInfo);
//    }
//
//    @Test
//    public void getExtraInfo3() {
//        String expectedInfo = extraInfo_value3;
//        //expectedInfo = extraInfo_value2;
//        assertEquals(l1.getExtraInfo().get(extraInfo_key3), expectedInfo);
//    }

    @Test
    public void constructorTest() {
        //
        Link link = new Link();
        String displayName = "howdy";
        link.setDisplayName(displayName);
        assertEquals(link.getDisplayName(), displayName);
        //
        //link = new Link(1L, displayName, url1, target1, null);
        link = new Link(1L, displayName, url1, target1);
        assertEquals(link.getDisplayName(), displayName);
    }

}
