package org.example.web.playground.notes.models;

import org.junit.Before;
import org.junit.Test;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertThat;

public class CustomLinkTest {

    public CustomLink customLink;
    //public CustomLink l2;

    @Before
    public void setUp () {
        customLink = new CustomLink();
        customLink.setHostname("www.example.org");
        customLink.setProto(CustomLink.Protocol.secure);
        customLink.setPath("aap/noot/mies");
        //System.out.println("-=- setup before each linkTest -=- customLink: " + customLink.toString() );
    }

    @Test
    public void linkToString () {
        String url;
        url = customLink.toString();
        assertEquals("https://www.example.org/aap/noot/mies", url);
    }

    @Test
    public void getHostname () {
        assertEquals("www.example.org", customLink.getHostname());
    }

    @Test
    public void uriCompTest () {
        //UriComponents uri =
        UriComponents uriComponents = UriComponentsBuilder
                .newInstance()
                .host("www.example.org")
                .scheme("https")
                .path("aap/noot/mies")
                .build();
        assertEquals("https://www.example.org/aap/noot/mies", uriComponents.toString());
    }
}
