package org.example.web.playground.notes.models;

import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class NoteTest {

    @Test
    public void getSetTitle() throws Exception {
        Note n1 = new Note();
        String testTitle = "howdy, my test title!";
        //
        n1.setTitle(testTitle);
        assertEquals(n1.getTitle(), testTitle);
    }

    @Test
    public void getSetContent() throws Exception {
        Note n1 = new Note();
        String testContent = "lorem ipsum: bla bla bla..";
        //
        n1.setContent(testContent);
        assertEquals(n1.getContent(), testContent);
    }

//    @Test
//    public void getSetTags() throws Exception {
//        Note n1 = new Note();
//        List<String> testTags = new ArrayList<>();
//        testTags.add("one");
//        testTags.add("two");
//        testTags.add("three");
//        n1.setTags(testTags);
//        //assertEquals(n1.getTags(), testTags.add("four"));
//        assertEquals(n1.getTags(), testTags);
//        //assertEquals(n1.getTags(), new String[]{"one", "two", "three"});
//    }

    @Test
    public void constructorTest() throws Exception {
        List<String> testTags = new ArrayList<>();
        testTags.add("one");
        testTags.add("two");
        //Note note = new Note(1L, "myTitle", "-=- my content -=-", testTags);
        Note note = new Note(1L, "myTitle", "-=- my content -=-");
        assertEquals(note.getTitle(), "myTitle");
        assertEquals(note.getContent(), "-=- my content -=-");
    }


    }
